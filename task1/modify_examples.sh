#!/bin/sh

cat<<EOT

EXAMPLES
_________________________________________________________

(dir) Example
 |_ first.txt
 |_ SecOnd.txt
 |_ THird.txt
 |_ direCTORY
    |_Fourth.txt
    |_FiFth.txt

./modify.sh -l TesTing.txt
result : 
	testing.txt

./modify.sh -u TesTing.txt
result : 
	TESTING.txt

./modify.sh -l Example
result :
	directory
	first.txt
	second.txt
	third.txt

./modify.sh -r -u Example
result : 
	EXAMPLE/DIRECTORY
	EXAMPLE/DIRECTORY/FOURTH.TXT
	EXAMPLE/DIRECTORY/FIFTH.TXT
	EXAMPLE/FIRST.TXT
	EXAMPLE/SECOND.TXT
	EXAMPLE/THIRD.TXT

./modify.sh 's/aaa/testing/' aaa1.txt
result :
	before sed command : aaa1.txt
	after sed command : testing1.txt
_________________________________________________________

INCORRECT EXAMPLES
_________________________________________________________

./modify.sh
result : 
	usage:
		modify.sh [-r|-h] [-l|-u|sed] <file/dir name>

./modify.sh -d
result :
	wrong argument

./modify.sh -l
result :
	dir/file name missing

./modify.sh -r
result :
	next command not found

EOT
