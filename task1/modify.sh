#!/bin/sh

flag=0

error_msg()
{
	echo "$1" 1>&2
}

#check if -r was in the command line
recursive()
{
	flag=1;
	if [ -z "$1" ]
	then
		error_msg "next command not found"
		exit 1
	fi	
}

lower()
{
	if [ -z "$1" ]
	then
		error_msg "dir/file name missing"
		exit 1
	fi

	#if $1 is a directory
	if [ -d $1 ]; then
		#if recursion
		if [ $flag = "1" ];
                then
			find $1/* | while read file; do echo "$file" | tr '[:upper:]' '[:lower:]'; done
			find $1/ -depth -execdir rename -f 'y/A-Z/a-z/' '{}' \;
                else
		#if no recursion
                #change the file/directory name first
                for file in $1/*;
                do
                        if [ "$1" = $(echo "$1" | tr '[:upper:]' '[:lower:]') ];
                        then
                                echo "already lowercase"
                                exit 1
                        fi
                        echo "$(basename "$file")" | tr '[:upper:]' '[:lower:]';
                        mv -i $file $1/"$(echo $(basename "$file") | tr '[:upper:]' '[:lower:]')";
                done
                #change the directory name
                mv -i $1 "$(echo $1 | tr '[:upper:]' '[:lower:]')";
                fi

	#if $1 is a file
	elif [ -f $1 ]; then
		echo "$1" | tr '[:upper:]' '[:lower:]'
		mv "$1" "$(echo $1 | tr '[:upper:]' '[:lower:]')"
	else
		echo "$1 is not valid"
		exit 1
	fi
}

upper()
{
	if [ -z "$1" ]
	then
		error_msg "dir/file name missing"
		exit 1
	fi

        #if $1 is a directory
        if [ -d $1 ]; then
                #if recursion
                if [ $flag = "1" ];
                then
                        find $1/* | while read file; do echo "$file" | tr '[:lower:]' '[:upper:]'; done
                        find $1/ -depth -execdir rename -f 'y/a-z/A-Z/' '{}' \;
                else
                #if no recursion
		#change the file/directory name first
                for file in $1/*;
                do
			if [ "$1" = $(echo "$1" | tr '[:lower:]' '[:upper:]') ]; 
			then
				echo "already uppercase"
				exit 1
			fi
                        echo "$(basename "$file")" | tr '[:lower:]' '[:upper:]';
                       	mv -i $file $1/"$(echo $(basename "$file") | tr '[:lower:]' '[:upper:]')";
                done
		#change the directory name
		mv -i $1 "$(echo $1 | tr '[:lower:]' '[:upper:]')";
                fi

        #if $1 is a file
        elif [ -f $1 ]; then
                echo "$1" | tr '[:lower:]' '[:upper:]'
                mv "$1" "$(echo $1 | tr '[:lower:]' '[:upper:]')"

        else
                echo "$1 is not valid"
                exit 1
	fi
}

sed_command()
{
	#if arg is file
	if [ -f "$2" ]
	then
		echo "before sed command : $2" 
		echo "after sed command : $2" | sed "$1"
		mv -i $2 "$(echo $2 | sed "$1")";
	fi

	#if arg is directory
	if [ -d "$2" ]
	then
		#if recursion
		if [ $flag = "1" ];
                then
			#echo "recursive"
                        find $2/* | while read file;
			do
				if [ $file != "$(echo $file | sed "$1")" ]
				then
					echo "before sed command : $file";
					echo "after sed command : $file" | sed "$1";
					mv -i $file "$(echo $file | sed "$1")"
				fi
	       		done

		#if no recursion
                else
                for file in $2/*;
                do
                        echo "$(basename "$file")" | sed "$1";
			mv -i $file "$2/$(echo $(basename "$file") | sed "$1")";
			mv -i $2 "$(echo $2 | sed "$1")";
                done
                fi
	fi
}

help()
{
cat<<EOT 1>&2
usage:
	$name [OPTION] <file/dir name>
	-r/-l must be in the command
-r
	recursively shows file/dir names
-l
	shows the lowercase of file/dir names
-u
	shows the uppercase of file/dir names
-h
	shows the current message; help
EOT
}

if [ -z "$1" ]
then
	cat<<EOT 1>&2
usage:
	$name [-r|-h] [-l|-u|sed] <file/dir name>
EOT
fi

while test "x$1" != "x"
do
	case "$1" in
		-h) help; exit 1;;
		-r) recursive "$2";;
		-l) lower "$2";;
		-u) upper "$2";;
		*) sed_command "$@";;
		-*) error_msg "wrong argument"; exit 1;;
	esac
	shift
done
	
