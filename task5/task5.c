#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/sem.h>
#include <unistd.h>
#include <signal.h>

#define PHIL_CNT 5
#define EAT_TIME 1
#define THINK_TIME 3
#define THINKING 0
#define HUNGRY 1
#define EATING 2
#define LEFT (process_no + 4) % PHIL_CNT
#define RIGHT (process_no + 1) % PHIL_CNT

int eat_cnt;
int semset;
int process_no;
char *philosopher_states;

void sem_init(int semnr, int val)
{
	int erg;
	erg=semctl(semset, semnr, SETVAL, val);
}

void sem_signal(int semnr)
{
	struct sembuf semops;
	int erg;
	
	semops.sem_num=semnr;
	semops.sem_op=1; //increase
	semops.sem_flg=0;

	//changes value of semaphore
	erg=semop(semset, &semops, 1);
}

void sem_wait(int semnr)
{
	struct sembuf semops;
	int erg;

	semops.sem_num=semnr;
	semops.sem_op=-1; //decrease
	semops.sem_flg=0;

	erg=semop(semset, &semops, 1);
}

void sigTermHandler()
{
	printf("Philosopher %d has eaten %d times.\n", process_no, eat_cnt);
	exit(0);
}

void test_and_eat(int p)
{
	if(philosopher_states[p]==HUNGRY && philosopher_states[LEFT]!=EATING
			&& philosopher_states[RIGHT]!=EATING)
	{
		philosopher_states[p]=EATING;
		sem_signal(p);
	}
}

void grab_forks(int left_fork_id)
{
	//to use semaphores first decrease sempahore value then increases the value
	sem_wait(PHIL_CNT);
	printf("Philosopher %d is hungry.\n", left_fork_id);
	philosopher_states[left_fork_id]=HUNGRY;
	test_and_eat(left_fork_id);
	sem_signal(PHIL_CNT);
	sem_wait(process_no);
}

void put_away_forks(int left_fork_id)
{
	sem_wait(PHIL_CNT);
	philosopher_states[left_fork_id]=THINKING;
	test_and_eat(LEFT);
	test_and_eat(RIGHT);
	sem_signal(PHIL_CNT);
}

void child()
{
	eat_cnt=0;
	//^C
	signal(SIGINT, sigTermHandler);

	printf("Philosopher %d joined the table. \n", process_no);

	while(1)
	{
		printf("Philosopher %d is thinking.\n", process_no);
		sleep(THINK_TIME);
		grab_forks(process_no);
		printf("Philosopher %d is eating.\n", process_no);

		eat_cnt+=1;
		sleep(EAT_TIME);
		put_away_forks(process_no);
	}
}

void parent()
{
	wait(NULL);
}

int main(int ac, char* av[])
{
	int shmID, i;

	//get shared memory from the os
	shmID=shmget(IPC_PRIVATE, PHIL_CNT, IPC_CREAT | 0x1ff);
	//shared memory attach
	//put into process adress shared memory's id
	philosopher_states=(char*)shmat(shmID, NULL, 0);

	//get a system semaphore set identifier
	semset=semget(IPC_PRIVATE, PHIL_CNT, IPC_CREAT | 0x1ff);
	if(semset==-1)	exit(1);

	for(i=0; i<PHIL_CNT; i++)
		sem_init(i, 0);

	for(i=0; i<PHIL_CNT; i++)
		philosopher_states[i]=THINKING;

	int pid;
	for(process_no=0; process_no<PHIL_CNT; process_no++)
	{
		pid=fork();
		if(pid==0)	break;
	}

	if(pid==0)
	{
		child();
	}
	else if(pid<0)
	{
		exit(1);
	}
	else
	{
		parent();
	}

	return 0;
}

