#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define BUF_SIZE 1024
 
//file that will be read & write
int fd_from, fd_to;
//shows whether the operand exists or not
int flg=0;

//shows error message when errflg is raised
void error_handling(char *message)
{
        fputs(message, stderr);
        fputc('\n', stderr);
        exit(1);
}

//copy the file to another file using read and write
void copy_read_write(int fd_from, int fd_to)
{
	//the array that is used when reads the contents of the file 
        char buf[BUF_SIZE]="";

	//reads the size of buf
	ssize_t bufsize=read(fd_from, buf, (ssize_t)sizeof(buf));

	//reads into the file as the size of bufsize
        if(read(fd_from, buf, sizeof(buf))==-1)
                error_handling("read() error\n");
	//writes into the file as the size of bufsize to prevent garbage values
        if(write(fd_to, buf, bufsize)==-1)
                error_handling("write() error\n");

	//close files after read&write
        close(fd_from);
        close(fd_to);

        fflush(stdin);
}

//copy the file to another file using mmap
int copy_mmap(int fd_from, int fd_to)
{
        char *src, *dest;
        size_t filesize;

	//finds the end of the read file to save it in the filesize
        filesize=lseek(fd_from, 0, SEEK_END);

	//creates the memory map
	//requests the kernel to map the fd to memory as the size
        src=mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, fd_from, 0);
	
	//changes the size of the file to the size selected
        ftruncate(fd_to, filesize);
        dest=mmap(NULL, filesize, PROT_READ|PROT_WRITE, MAP_SHARED, fd_to, 0);

	//copies the value of the memory
        memcpy(dest, src, filesize);

	//unmap pages of memory
        munmap(src, filesize);
        munmap(dest, filesize);

	//close files after copying
        close(fd_from);
        close(fd_to);

        return 0;
}

//is used when -h is called
void printusage() {
	printf("\n");
	printf("usage : ./task6 [-mh] [filename] [filename] \n");
	printf("\n");
}

//open the files
void open_file(char* file1, char* file2)
{
	//open read only
	fd_from=open(file1, O_RDONLY);
	if(fd_from==-1)	error_handling("file_from open() error\n");
	//open read&write/ create file if the file does not exist
	fd_to=open(file2, O_RDWR|O_CREAT, 0666);
	if(fd_to==-1)	error_handling("file_to open() error\n");
	//without -m
	if(flg==0)	copy_read_write(fd_from, fd_to);
	//with -m
	else	copy_mmap(fd_from, fd_to);
}
 
int main(int argc, char *argv[]) {
	//c : an integer that is used to for getopt
   	int c;
	//raise the errflg if there is an error(unrecognized operand)
    	int errflg=0;
	char *ifile, *ofile;
	//the following element after the operand
	extern char *optarg;
	//optind : the location of the options
	//optopt : shows the element that was not in the option
	extern int optind, optopt;
	//saves the input & output filenames
	char tmp[BUF_SIZE][BUF_SIZE];

	//gets elements that is in args
    	while ((c = getopt(argc, argv, "hm:")) != -1) {
        	switch(c) {
        		case 'm':
				//saves the file that will be read
				strcpy(tmp[1], optarg);
				//raises flag to show if there is another element besides the files to be read(operand)
	    			flg=1;
            			break;
        		case 'h':
            			printusage();
            			return 0;
            			break;
        		/*case ':':
            			fprintf(stderr, "-%c option needs a operand\n", optopt);
            			errflg++;
            			break;*/
        		case '?':
            			fprintf(stderr, "-%c option is unrecognizable\n", optopt);
	    			errflg++;
            			break;
        	}
    	}
     
	//if there is an error
    	if(errflg)	return 1;

	//
    	for (; optind < argc; optind++) {
		//when an operand -m is included
		//saves the file name that will be wrote in tmp[2]
		if(flg==1)	strcpy(tmp[2], argv[optind]);
		//when operand is not included
		//saves the input&output file name into tmp
		else	strcpy(tmp[optind], argv[optind]);
    	}

	open_file(tmp[1], tmp[2]);

    	return 0;
}
