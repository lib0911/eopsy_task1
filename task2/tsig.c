#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>

//Message about execution completion
void read_childproc(int sig)
{
	int status; 
	pid_t id=waitpid(-1, &status, WNOHANG);

	if(WIFEXITED(status))
	{
		printf("Remove proc id : %d\n", id);
		printf("Child send : %d\n", WEXITSTATUS(status));
	}
}

int main(int ac, char* av[])
{
	pid_t pid, ppid;
	int i, status;
	struct sigaction act;

	act.sa_handler=read_childproc;
	sigemptyset(&act.sa_mask);
	act.sa_flags=0;
	sigaction(SIGCHLD, &act, 0);

	for(i=0; i<5; i++){
		pid=fork();
		sleep(1);

		//if child process was not correctly created
		if(pid==-1)
		{
			ppid=getppid();

			kill(ppid, SIGUSR1);
			printf("ERROR; Child process was not created");
			exit(1);
		}
		else if(pid==0) //child process
		{
			ppid=getppid();

			printf("My parent process is %d\n", ppid);
			sleep(10);
			exit(10);
		}
		else //parent process
		{
			printf("Child process %d created\n", pid);
		}
	}

	//try to wait for any children while there exits at least one
	while((pid=waitpid(-1, &status, 0))!=-1)
	{
		printf("Process %d terminated\n", pid);
	}
}
