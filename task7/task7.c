/****** Task 7 *******/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>

#define N 5
#define THINKING 0
#define HUNGRY 1
#define EATING 2
#define LEFT (i + N - 1) % N
#define RIGHT (i + 1) % N

int state[N];
pthread_mutex_t mutex;
pthread_cond_t s[5];
pthread_t phil[5];

int identity[5]={0, 1, 2, 3, 4};

void test(int i)
{
	pthread_mutex_lock(&mutex);

	if(state[i] == HUNGRY
		&& state[LEFT] != EATING
		&& state[RIGHT] != EATING) {
		state[i] = EATING;
		printf("Philosopher %d takes fork %d and %d\n",
				i + 1, LEFT + 1, i + 1);
		printf("Philosopher %d is Eating\n", i + 1);
		pthread_cond_signal(&s[i]);
	}
	pthread_mutex_unlock(&mutex);
}

void* grab_forks(int i)
{
        pthread_mutex_lock(&mutex);
        state[i] = HUNGRY;
        printf("Philosopher %d is hungry\n", i + 1);
        pthread_mutex_unlock(&mutex);

        test(i);
        pthread_mutex_lock(&mutex);
        if(state[i] != EATING)
        {
		//block until the signal is sent by cond
                pthread_cond_wait(&s[i], &mutex);
        }
        pthread_mutex_unlock(&mutex);

        return NULL;
}

void* put_away_forks(int i)
{
        pthread_mutex_lock(&mutex);
        state[i] = THINKING;
        printf("Philosopher %d putting fork %d and %d down\n",
                        i + 1, LEFT + 1, i + 1);
        printf("Philosopher %d is thinking\n", i + 1);
        pthread_mutex_unlock(&mutex);
	//test whether philosophers of both sides are hungry
        test(LEFT);
  	test(RIGHT);
}

//thread function
void* philosopher(void* num)
{
	while(1) {
		
		int* i = num;
		sleep(1);
		grab_forks(*i);
		sleep(1);
		put_away_forks(*i);
	}
}

int main()
{
	pthread_mutex_init(&mutex, NULL);

	int i=0;
	for(i = 0; i < N; i++)
	{
		state[i]=THINKING;
		//initialize condition variable
		pthread_cond_init(&s[i], NULL);
	}

	for(i = 0; i < N; i++)
	{
		//create philosopher
		pthread_create(&phil[i], NULL, philosopher, &identity[i]);
		printf("Philosopher %d is thinking\n", i + 1);
	}
	for(i = 0; i < N; i++)
		//wait until the thread is ended
		pthread_join(phil[i], NULL);

	return 0;
}

